package com.example.atomasg.excelpoi2;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Library from project on Github: https://github.com/centic9/poi-on-android
 *
 * Included file:  poishadow-all.jar   in   app/libs
 * Version: 0.5
 */

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private String TEST_FILE_NAME = "pruebaExcel.xlsx";
    private String PT_FILE_NAME = "SolicitudPT.xlsx";

    private Button btnRead, btnUpdate, btnAdd, btnReadPT, btnReadPTIntent;
    private TableLayout table;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Necessary System-Properties
        System.setProperty("org.apache.poi.javax.xml.stream.XMLInputFactory", "com.fasterxml.aalto.stax.InputFactoryImpl");
        System.setProperty("org.apache.poi.javax.xml.stream.XMLOutputFactory", "com.fasterxml.aalto.stax.OutputFactoryImpl");
        System.setProperty("org.apache.poi.javax.xml.stream.XMLEventFactory", "com.fasterxml.aalto.stax.EventFactoryImpl");


        btnRead = (Button) findViewById(R.id.btnRead);
        btnAdd = (Button)findViewById(R.id.btnAdd);
        btnUpdate = (Button)findViewById(R.id.btnUpdate);
        btnReadPT = (Button)findViewById(R.id.btnReadPT);
        btnReadPTIntent = (Button)findViewById(R.id.btnReadPTIntent);
        table = (TableLayout) findViewById(R.id.table);

        btnReadPT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    readWorkbook(PT_FILE_NAME);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        btnReadPTIntent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                readExcelIntent();
            }
        });

        btnRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    readWorkbook(TEST_FILE_NAME);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    createNewXlsx();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    updateWorkbook();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void readExcelIntent() {
        //Get the file
        File yourDir = new File(Environment.getExternalStorageDirectory(), PT_FILE_NAME);
        //Get the path
        Uri uri = Uri.fromFile(yourDir);
        Log.d(TAG, uri.toString());
        Intent intentXlsx = new Intent(Intent.ACTION_VIEW);
        intentXlsx.setDataAndType(uri, "application/vnd.ms-excel");
        intentXlsx.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(Intent.createChooser(intentXlsx, "Choose an application to open with:"));
    }

    private void readWorkbook(String nameFile) throws FileNotFoundException {
        File file = null;
        if (isExternalStorageAvailable()) {
            String baseDir = Environment.getExternalStorageDirectory().getAbsolutePath();
            file = new File(baseDir, nameFile);
        }
        if (file != null) {
            FileInputStream fis = new FileInputStream(file);
            XSSFWorkbook workbook = null;
            try {
                workbook = new XSSFWorkbook(fis);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (workbook != null) {
                //Reset TableView
                table.removeAllViews();

                CreationHelper helper = workbook.getCreationHelper();
                FormulaEvaluator evaluator = helper.createFormulaEvaluator();
                //Get only first sheet
                XSSFSheet sheet = workbook.getSheetAt(0);
                for (int r = 0; r <= sheet.getLastRowNum(); r++) {//add to table new row
                    TableRow tRow = addNewRow();
                    XSSFRow row = sheet.getRow(r);
                    if (row != null)
                        for (int x = 0; x <= row.getLastCellNum(); x++) {//add to table new cell
                            Cell cell = row.getCell(x);
                            addNewCell(tRow, evaluator.evaluateInCell(cell));
                        }
                }
            }


            try {
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

    }

    private void addNewCell(TableRow row, Cell cell) {
        TextView textV = new TextView(this);
        textV.setText("");
        textV.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));
        textV.setPadding(5, 5, 5, 5);
        if (cell != null)
            switch (cell.getCellTypeEnum()) {
                case BLANK:
                    textV.setText("");
                    break;
                case STRING:
                    textV.setText(cell.getStringCellValue());
                    break;
                case BOOLEAN:
                    if (cell.getBooleanCellValue()) {
                        textV.setBackgroundColor(android.graphics.Color.GREEN);
                        textV.setText("*True*");
                    } else {
                        textV.setBackgroundColor(android.graphics.Color.RED);
                        textV.setText("*False*");
                    }
                    break;
                case NUMERIC:
                    textV.setText(String.valueOf(cell.getNumericCellValue()));
                    break;
                case FORMULA:   //Never reached case
                    textV.setText(cell.getCellFormula());
                    break;
                default:
                    Log.e(TAG, "addNewCell: Cell type unknown");
            }
        row.addView(textV);
    }

    private TableRow addNewRow() {
        TableRow row = new TableRow(this);
        row.setGravity(Gravity.CENTER);
        row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 1.0f));
        table.addView(row);
        return row;
    }


    private void createNewXlsx() throws java.io.IOException {
        File file = null;
        if (isExternalStorageAvailable()) {
            String baseDir = Environment.getExternalStorageDirectory().getAbsolutePath();
            file = new File(baseDir, TEST_FILE_NAME);
            file.createNewFile();   //Create if not exist
        }
        if (file != null) {
            FileInputStream fis = new FileInputStream(file);
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet("sheet 1");

            //Create First Row
            XSSFRow row1 = sheet.createRow(0);
            XSSFCell r1c1 = row1.createCell(0);
            r1c1.setCellValue("Emd Id");
            XSSFCell r1c2 = row1.createCell(1);
            r1c2.setCellValue("NAME");
            XSSFCell r1c3 = row1.createCell(2);
            r1c3.setCellValue("AGE");

            //Create Second Row
            XSSFRow row2 = sheet.createRow(1);
            XSSFCell r2c1 = row2.createCell(0);
            r2c1.setCellValue("1");
            XSSFCell r2c2 = row2.createCell(1);
            r2c2.setCellValue("Ram");
            XSSFCell r2c3 = row2.createCell(2);
            r2c3.setCellValue("20");

            //Create Third Row
            XSSFRow row3 = sheet.createRow(2);
            XSSFCell r3c1 = row3.createCell(0);
            r3c1.setCellValue("2");
            XSSFCell r3c2 = row3.createCell(1);
            r3c2.setCellValue("Shyam");
            XSSFCell r3c3 = row3.createCell(2);
            r3c3.setCellValue("25");
            fis.close();
            FileOutputStream fos = new FileOutputStream(file);
            workbook.write(fos);
            fos.close();
            Log.d(TAG, "createNewXlsx: DONE");
        } else {
            Log.e(TAG, "createNewXlsx: File not found");
        }
    }


    private void updateWorkbook() throws java.io.IOException {      //Write exiting file
        File file = null;
        if (isExternalStorageAvailable()) {
            String baseDir = Environment.getExternalStorageDirectory().getAbsolutePath();
            file = new File(baseDir, TEST_FILE_NAME);
            file.createNewFile();       //If doesn´t exist then create.
        }
        if (file != null) {
            FileInputStream fis = new FileInputStream(file);
            XSSFWorkbook workbook = null;
            workbook = new XSSFWorkbook(fis);
            XSSFSheet sheet = workbook.getSheetAt(0);


            //Create First Row example, create and replace row
            XSSFRow row1 = sheet.createRow(0);
            XSSFCell r1c1 = row1.createCell(0);
            r1c1.setCellValue("Updatess");
            XSSFCell r1c2 = row1.createCell(1);
            r1c2.setCellValue("NAME");
            XSSFCell r1c3 = row1.createCell(2);
            r1c3.setCellValue("AGE");


            //Create Second Row example, get row and replace with numeric cells
            XSSFRow row2 = sheet.getRow(1);
            XSSFCell r2c1 = row2.createCell(0);
            r2c1.setCellType(CellType.NUMERIC);
            r2c1.setCellValue(3);
            XSSFCell r2c2 = row2.createCell(1);
            r2c2.setCellType(CellType.NUMERIC);
            r2c2.setCellValue(2);

            //Create Third Row example
            XSSFRow row3 = sheet.getRow(2);
            XSSFCell r3c1 = row3.createCell(0);
            r3c1.setCellValue("2");
            XSSFCell r3c3 = row3.createCell(2);
            r3c3.setCellValue("21115");

            //Create fourth Row example, create row and add data
            XSSFRow row4 = sheet.createRow(3);
            XSSFCell r4c3 = row4.createCell(2);
            r4c3.setCellValue("celda 4,3");

            //Create fifth Row example, create roy ando

            XSSFRow row6 = sheet.getRow(5);
            if (row6 == null) {
                row6 = sheet.createRow(5);
            }
            XSSFCell r6c2 = row6.createCell(1);
            r6c2.setCellType(CellType.FORMULA);
            r6c2.setCellFormula("SUM(A2:B2)");


            fis.close();
            FileOutputStream fos = new FileOutputStream(file);
            workbook.write(fos);
            fos.close();
            Log.d(TAG, "updateWorkbook: DONE");
        }else{
            Log.e(TAG, "updateWorkbook: File not found");
        }
    }

    private static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }
}
